# LAMP Docker #

Un LAMP que incluye composer, para desarrollo en ambiente php 7.2 + apache 2 + mysql 5.7 y añadiendo Postgresql 9.6

### Uso de docker y docker-compose

### Tecnologias utilizadas ###

* Docker
* Docker-compose en Linux
* Imágenes (LAMP)
* (NUEVO)Postgres y PgMyAdmin en docker-compose paralelo

### Ejecución ###
1. Pulee el proyecto: git pull https://gitlab.com/carlorregod/docker-lamp.git
2. Dentro del directorio ejecute: docker-compose up -d
3. Si desea postgres, dentro de la carpeta postgres: docker-compose up -d
4. Finalice docker compose con: docker-compose down
5. Aloje sus archivos en la carpeta "src"
6. Si existen errores de permisos, abra una terminal y dentro de este proyecto, ejecute docker exec -it lamp-cod /bin/bash
7. Dento de esa terminal use chmod -R 7XY para cambiar los permisos recomiendo 765,chmod -R 765)

### Mantención de imágenes y contenedores Docker ###

crontab -e

Y dentro copie y pegue al final:

>\# Elimina los contenedores en estado 'Exit' cada Domingo a las 3:30 de la madrugada

30 3 * * 6 /usr/bin/docker rm $(sudo docker ps -a | grep Exit | cut -d ' ' -f 1) 
>
> \#Elimina las imágenes antiguas cada Domingo a las 3:35 de la madrugada

35 3 * * 6 /usr/bin/docker rmi -f $(docker images | tail -n +2 | awk '$1 == "<none>" {print $'3'}')
>
\#Elimina los volúmenes huérfanos cada Domingo a las 3:40 de la madrugada

40 3 * * 6 /usr/bin/docker volume rm $(docker volume ls -qf dangling=true)



###  ( ͡° ͜ʖ ͡°) ###

* ( ͡° ͜ʖ ͡°)
* ( ͡° ͜ʖ ͡°)
* ( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)
* ( ͡° ͜ʖ ͡°)
