PARA EJECUTAR
Ejecute
Antes de ejecutar cualquier cosa, ejecute el comando sólo la primera vez:
docker build . -t carlorregod/lamp:1.0.1

Iniciando:
docker-compose up -d

Finalizando
docker-compose down

Si lo anterior no resulta, ejecute
docker-compose kill

AlTERNATIVA DE EJECUCION
Carpeta scripts:
Iniciar con
inicio.sh (desde la terminal)
Terminar con
cerrar.sh

PARA ENTRAR O SALIR DE LA TERMINAL DEL SERVIDOR
Ejecute 
docker exec -it lamp-cod /bin/bash

MANTENCION
Elimando containers exited
docker rm $(sudo docker ps -a | grep Exit | cut -d ' ' -f 1)
Eliminado imagenes <none>
docker rmi -f $(docker images | tail -n +2 | awk '$1 == "<none>" {print $'3'}')
Eliminar volúmenes huérfanos
docker volume rm $(docker volume ls -qf dangling=true)

MANTENCIÓN AUTOMÁTICA
crontab -e
Y dentro copie y pegue al final:
# Elimina los contenedores en estado 'Exit' cada Domingo a las 3:30 de la madrugada
30 3 * * 6 /usr/bin/docker rm $(sudo docker ps -a | grep Exit | cut -d ' ' -f 1) 

# Elimina las imágenes antiguas cada Domingo a las 3:35 de la madrugada
35 3 * * 6 /usr/bin/docker rmi -f $(docker images | tail -n +2 | awk '$1 == "<none>" {print $'3'}')

# Elimina los volúmenes huérfanos cada Domingo a las 3:40 de la madrugada
40 3 * * 6 /usr/bin/docker volume rm $(docker volume ls -qf dangling=true)


